import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ListPost from "./components/ListPost";
import Post from "./components/Post";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={ListPost} />
        <Route path="/tutorials/:id" component={Post}/>
      </Switch>
    </Router>
  );
}

export default App;
