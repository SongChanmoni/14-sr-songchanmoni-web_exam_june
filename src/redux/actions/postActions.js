import Axios from "axios";

export const GET_POST = "GET_POSTS"
export const GET_POST_BY_ID = "GET_POST_BY_ID"

export const fetchgetPosts = () => {
  return async (dispatch) => {
     const result = await Axios.get("http://110.74.194.124:3034/api/tutorials");
     dispatch({
      type: GET_POST,
      data: result.data,
    });
  };
};

export const fetchgetPostById = (id) => {
  return async (dispatch) => {
    const result = await Axios.get(`http://110.74.194.124:3034/api/tutorials/${id}`);
  
    dispatch({
      type: GET_POST_BY_ID,
      data: result.data,
    });
  };
};
