import { GET_POST,GET_POST_BY_ID } from "../actions/postActions"

const initState = {
  data: [],
  post: null
}

export const postReducer = (state = initState, action) => {
  switch(action.type) {
    case GET_POST:
      return {
        ...state,
        data: action.data
      }
    case GET_POST_BY_ID:
      return {
        ...state,
        post: action.data
      }
    default:
      return state
  }
}