import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchgetPostById } from "../redux/actions/postActions";
import { withRouter } from "react-router-dom";

class Post extends React.Component {
  componentWillMount() {
    let id = this.props.match.params.id;
    this.props.fetchgetPostById(id);
  }

  render() {
    if (this.props.tutorials) {
      return (
        <div className="PostWrapper">
          <h1>Title : {this.props.tutorials.title}</h1>
        </div>
      );
    }
    return <div className="PostWrapper"></div>;
  }
}

const mapStateToProps = (state) => {
  return {
    post: state.post,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      fetchgetPostById,
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Post));
