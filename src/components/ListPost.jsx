import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {fetchgetPostById ,fetchgetPosts } from "../redux/actions/postActions";
import { Link } from "react-router-dom";

const ListPost = (props) => {
  useEffect(() => {
    props.fetchgetPosts();
  }, []);
  

  return (
    <div>
      <h1>All Tutorials</h1>
      {
        props.data.map((tutorials) => {
          return (
            <h3 key={tutorials.id}>
              Tutorials : {tutorials.id}
              {" : "}
              <Link to={`/tutorials/${tutorials.id}`}>{tutorials.title}</Link>
            </h3>
          );
        })
      }
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      fetchgetPosts,
      fetchgetPostById,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ListPost);
